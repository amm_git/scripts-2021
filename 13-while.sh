#! /bin/bash
#---------
# 7 numerar stdin linia a linia i a majúscules
num=1
while read -r line
do
	echo "$line" | tr 'a-z' 'A-Z'
	((num++))
done
exit 0

# 6 processar stdin fins al token FI
read -r line
while [ "$line" != "FI" ]
do
	echo "$line"
	read -r line
done
exit 0

# 5 processar stdin i mostra numerada la linia
comptador=1
while read -r line
do
	echo "$comptador: $line"
	((comptador++))
done
exit 0

# 4 processar stdin
while read -r line
do
	echo $line
done
exit 0

# 3 iterar arguments arguments
while [ -n "$1" ]
do
  echo "$1, $#, $*"
  shift
done
exit 0




# 2 compte enrere
comptador=$1
MIN=0
while [ $comptador -ge $MIN ]
do
  echo -n "$comptador, "
  ((comptador--))
done
exit 0

# 1
MAX=10
inici=1
while [ $inici -le $MAX ]
do
  echo "$inici"
  ((inici++))
done
exit 0
