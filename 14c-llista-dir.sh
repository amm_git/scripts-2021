#! /bin/bash
#------------
# per cada element del directori, dir si es regular, link, dir o altres

#numerar els fitxers
num=1
# existeix un arg
if [ $# -ne 1 ]
then
  echo "Error, nºargs != 1"
  exit 1
fi

# dir és un directori
dir=$1

if ! [ -d $dir ]
then
  echo "dir no és un directori"
  echo "usage: $0 dir"
  exit 2
fi


llistat=$(ls $dir)
for fit in $llistat
do
  if [ -f $dir/$fit ]
  then
   echo "$fit regular file"
  elif [ -d $dir/$fit ]
  then
   echo "$fit directory"
  elif [ -h $dir/$fit ]
  then
   echo "$fit link"
  else
   echo "$fit altres"
  fi
done
exit 0
