#! /bin/bash
# Arnau Morraja 
# M01 ASIX1 @j19
# -----------------------------

#llistar logins del /etc/passwd numerats
num=1
logins=$(cut -d: -f1 /etc/passwd | sort)
for arg in $logins
do
  echo "$num: $arg"
  ((num++))
done
exit 0



num=1
llistat=$(ls)
for arg in $llistat
do
  echo "$num: $arg"
  ((num++))
done
exit 0

llistat=$(ls)
num=1
for arg in $*
do
  echo "$num: $arg"
  num=$((num+1))
done
exit 0

#iterar per llista d'arguments
for arg in "$@"
do
  echo $arg
done
exit 0

#iterar per valor de variable

for nom in $llistat
do
  echo "$nom"
done
exit 0

#iterar per un conjunt d'elements
for nom in "pere pau marta anna"
do
  echo "$nom"
done
exit 0
