#! /bin/bash
# Arnau Morraja M01
#-----------------
ERR_NARGS=1
ERR_NOTA=2

#validar 1 argument
if [ $# -ne 1 ]
then
  echo "Error: nº arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi

nota=$1

#validar 0-10
if [ $nota -lt 0 -o $nota -gt 10 ]
then
  echo "Error: rang incorrecte"
  echo "Usage: 0 <= nota <= 10"
  exit $ERR_NOTA
fi

#codi
if [ $nota -ge 5 ]
then
  echo "nota $nota aprovat"
  exit 0
else
  echo "nota $nota suspès"
fi
