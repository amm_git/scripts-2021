#! /bin/bash
#
#-----------

#almenys 1 argument
#per cada nota, dir suspes, aprovat...


#validar almenys 1 argument 
ERR_NARGS=1

if [ $# -eq 0 ]
then
  echo "Error: nºarguments incorrecte"
  exit $ERR_NARGS
fi


#bucle for
for nota in $*
do
  if ! [ $nota -ge 0 -a $arg -le 10 ]
  then
   echo "Error: nota $arg no vàlida. Ha de ser al rang 0-10" >> /dev/stderr
 
  elif [ $nota -lt 5 ]
  then
   echo "Suspès"

  elif [ $nota -lt 7 ]
  then
   echo "Aprovat"

  elif [ $nota -lt 9 ]
  then
   echo "Notable"

  else
   echo "Excel·lent"
  fi
done
exit 0
