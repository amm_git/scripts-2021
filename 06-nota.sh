#! /bin/bash
# Arnau Morraja M01 @j19
# suspes, aprovat, notable, excellent

#----
ERR_NARGS=1
ERR_NOTA=2
OK=0

#validar nºarguments
if [ $# -ne 1 ]
then
  echo "Error: nº arguments incorrecte"
  echo "Usage: nota"
  exit $ERR_NARGS
fi
nota=$1

#validar rang
if [ $nota -lt 0 -o $nota -gt 10 ]
then
  echo "Error: rang incorrecte"
  echo "Usage: 0 <= nota <= 10"
  exit $ERR_NOTA
fi

#codi
if [ $nota -lt 5 ]
then
  echo "$nota suspès"
  exit $OK
fi

if [ $nota -eq 7 -o $nota -eq 8 ]
then
  echo "$nota notable"

elif [ $nota -ge 9 ]
then
  echo "$nota excel·lent"
  exit $OK

elif [ $nota -eq 5 ]
then
  echo "$nota suficient"
  exit $OK
else
  echo "$nota aprovat"
  exit $OK
fi  
