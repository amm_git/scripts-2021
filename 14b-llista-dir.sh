#! /bin/bash
#------------
#numerar els fitxers
num=1
# existeix un arg
if [ $# -ne 1 ]
then
  echo "Error, nºargs != 1"
  exit 1
fi

# dir és un directori
dir=$1

if ! [ -d $dir ]
then
  echo "dir no és un directori"
  echo "usage: $0 dir"
  exit 2
fi


llistat=$(ls $dir)
for fit in $llistat
do
  echo "$num: $fit"
  ((num++))
done
exit 0
