#! /bin/bash
# Arnau Morraja M01
# 
#-----------

#validar
if [ $# -ne 1 ]
then
  echo "Error: nombre d'arguments incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi

#codi
edat=$1

if [ $edat -ge 18 ]
then
  echo "edat $edat és major d'edat"
else
  echo "edat $edat és menor d'edat"
fi
exit 0
