#! /bin/bash
# Arnau Morraja 
# M01 ASIX1 @j19
# -----------------------------
llistat=$(ls)

#iterar per llista d'arguments
for arg in $*
do
  echo $arg
done
exit 0

#iterar per valor de variable

for nom in $llistat
do
  echo "$nom"
done
exit 0

#iterar per un conjunt d'elements
for nom in "pere pau marta anna"
do
  echo "$nom"
done
exit 0
